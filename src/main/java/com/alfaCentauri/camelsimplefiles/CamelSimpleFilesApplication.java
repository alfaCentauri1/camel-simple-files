package com.alfaCentauri.camelsimplefiles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamelSimpleFilesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamelSimpleFilesApplication.class, args);
	}

}
