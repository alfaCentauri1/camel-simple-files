# camel-simple-files

Ejemplo de aplicación de **Sripng web con Apache Camel**. 

## Requisitos

* JDK 18 ó superior.
* MySQL server 5.1 ó superior.
* Librerias de Maven.

***

## Instalación
* Compile el proyecto con el IDE de su preferencia.
* Ejecute desde la consola; en la raíz del proyecto, java -jar CamelSimpleFilesApplication.jar

## Name
Camel-simple-file2

## Description
### Ejemplo 1 de ruta:
```java
@Override
public void configure() throws Exception {
// crea una plantilla de ruta con el nombre dado
    routeTemplate("miPlantilla")
// aquí definimos los parámetros de entrada requeridos (pueden tener valores predeterminados)
        .templateParameter("name", "primera")
        .templateParameter("greeting")
        .templateParameter("periodo", "3s");
/* Aquí viene la ruta en la plantilla: observe cómo usamos {{name}} para referirnos a los parámetros de la
plantilla también podemos usar {{propertyName}} para referirnos a marcadores de posición de propiedad. */
    from("timer:{{name}}?period={{periodo}}")
        .setBody(simple("{{greeting}} ${body}"))
        .log("Timer Invoked and the bodys " + environment.getProperty("message"));
}
```
### Ejemplo 2 de ruta:
```java
public void configure() throws Exception {
    from("{{startRoute}}")
//Consulte la configuración de la ruta por el id que se utilizará para esta ruta.
        .routeConfigurationId("yamlError")
        .onException(Exception.class).handled(true)
        .log("WARN: ${exception.message}")
        .to("{{toRoute}}");
}
```

## Contributing
[GitHub: alfaCentauri](https://github.com/alfaCentauri)

## Authors and acknowledgment
[GitLab: alfaCentauri1](https://gitlab.com/alfaCentauri1)

## License
GNU version 3.

## Project status
Developer.
